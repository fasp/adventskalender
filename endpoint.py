from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app

import logging
from google.appengine.api import mail
import datetime
import os
import time

import sys
reload(sys)
sys.setdefaultencoding('utf-8')

class MainPage(webapp.RequestHandler):

    def get(self):
        logging.info("I have been summoned")
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('I have been summoned')

        sender_address='sender@url.com'
        recipients=["recipient_A_displayName <recipient_A@url.com>", "recipient_B_displayName <recipient_B@url.com>"]
        self.cron_method(sender_address, recipients)

    def buildMessageText(self, realDay, now, lines):
        timeStamp="Verschickt um {0:02}:{1:02} Uhr am {2:02}.{3:02}.".format(now.hour+1, now.minute, now.day, now.month)
        messageText=lines[realDay-1]+"\n"+"-"*20+"\n"+timeStamp
        return messageText

    def send_approved_mail(self, sender_address, recipients):

        now = datetime.datetime.now()
        day=now.day
        month=now.month
        if month==11 and day==30:
            messageText=INIT_MESSAGE
            sbj='Dein Adventskalender 2017'
            self.sendMail(sender_address, recipients, sbj, messageText)
        elif month==12 and day<=24:
            lines = [line.rstrip('\n') for line in open("kalender.txt", "r")]
            if len(lines)>=day-1:
                messageText=self.buildMessageText(day, now, lines)
                sbj="Adventskalender Tag %d" % now.day
                self.sendMail(sender_address, recipients, sbj, messageText)

    def sendMail(self, sender_address, recipients, sbj, messageText):
        message = mail.EmailMessage(
            sender=sender_address,
            subject=sbj)
        message.to = recipients
        message.body = messageText
        message.send()

    def cron_method(self, sender_address, recipients):
        # only allow internal ('x-') and scheduled '-Cron' requests
        if self.request.headers.get('X-AppEngine-Cron') is None:
            logging.info("error-- not cron")
            self.UNAUTH = "cron"
            logging.info("\n\t...but you're not authorized!")
            self.response.out.write("\t...but you're not authorized!")
        else:
            logging.info("Cron task executed")
            self.send_approved_mail(sender_address, recipients)

application = webapp.WSGIApplication([('/', MainPage), ('/cronRunner', MainPage)], debug=True)

def main():
    run_wsgi_app(application)

if __name__ == "__main__":
    main()
